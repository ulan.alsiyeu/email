package email

const (
	SentStatus      = "sent"
	DeliveredStatus = "delivered"
	FailedStatus    = "failed"
)

type Email struct {
	ID      int
	Address string
	Message string
}
