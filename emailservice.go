package email

import (
	"context"
	"fmt"
)

type EmailService struct{}

func (es *EmailService) Send(ctx context.Context, email *Email) error {
	fmt.Println(fmt.Sprintf("Email is sent with id = %d", email.ID))
	return nil
}

func (es *EmailService) CollectEmailsInfo(ctx context.Context) (map[string]int, error) {
	return map[string]int{SentStatus: 5, DeliveredStatus: 10, FailedStatus: 1}, nil
}
